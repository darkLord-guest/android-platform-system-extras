NAME = ext4fixup
SOURCES = ext4fixup_main.c
SOURCES := $(foreach source, $(SOURCES), ext4_utils/$(source))
CFLAGS += -Wno-unused-parameter
CPPLAGS +=  -Iext4_utils
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-Wl,-rpath-link=. \
	-L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-lz -lsparse -lsepol \
	-Ldebian/out -lext4_utils

build: $(SOURCES)
	mkdir --parents debian/out
	$(CC) $^ -o debian/out/$(NAME) $(CFLAGS) $(LDFLAGS)

