NAME = make_ext4fs
SOURCES = make_ext4fs_main.c
SOURCES := $(foreach source, $(SOURCES), ext4_utils/$(source))
CFLAGS += -DHOST
CPPFLAGS += -I/usr/include/android
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-Wl,-rpath-link=. \
	-L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-lz -lselinux -lsparse -lcutils \
	-Ldebian/out -lext_utils

build: $(SOURCES)
	mkdir --parents debian/out
	$(CC) $^ -o debian/out/$(NAME) $(CFLAGS) $(LDFLAGS)